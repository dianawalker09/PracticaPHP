<?php
	$un_bool = TRUE;			//VALOR BOOLEANO   
	$un_str="Programacion";		//UNA CADENA			
	$un_str2 ='Programacion';	//UNA CADENA
	$un_int = 12;				//UN ENTERO

	echo gettype($un_bool); //imprime:booleano
	echo gettype($un_str);  //imprime: string

	//si este valor es un entero, incrementarlo en cuatro
	if(is_int($un_int)){
		$un_int += 4;
	}

	//si $bool es una cadena, imprimirla
	// (no imprime nada)
	if(is_string($un_bool)){
		echo "cadena: $un_bool";
	}
?>