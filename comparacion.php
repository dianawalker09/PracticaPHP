<!DOCTYPE html>
<html>
<head>
	<title>Ejemplo de operadores de comparacion</title>
</head>
<body>
	<h1>Ejemplo de operaciones comparacion en PHP</h1>
	<?php
		$a = 8;
		$b = 3;
		$c = 3;
		echo $a == $b, "<br>"; //equivalente
		echo $a != $b, "<br>"; //no equivalente
		echo $a < $b, "<br>"; //menor que
		echo $a > $b, "<br>"; //mayor que
		echo $a >= $c, "<br>"; // mayor o igual
		echo $a <= $c, "<br>"; //menor o igual
	?>
</body>
</html>